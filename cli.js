#!/usr/bin/env node

const childProcess = require('child_process');
const os = require('os');

const spawn = cmd => new Promise((resolve, reject) => {
  const process = childProcess.spawn(cmd, {
    stdio: 'inherit',
    shell: true,
  });
  process.once('exit', (code, signal) => {
    if (code === 0) {
      resolve();
    } else {
      reject({ code, signal });
    }
  });
  process.once('error', err => {
    reject(err);
  });
});

const help = () => {
  console.log('Usage examples:');
  console.log('npx zaber-python-launcher -3 ...args');
  console.log('npx zaber-python-launcher -2.7 ...args');
  console.log('npx zaber-py -3.5 ...args');
  process.exit(1);
};

const argv = process.argv.slice(2);
if (argv.length === 0) {
  help();
}
const versionArg = argv[0].match(/^-(\d+(\.\d+)?)$/);
if (!versionArg) {
  help();
}
argv.shift();

let cmd = [];
if (os.platform() === 'win32') {
  cmd.push('py', versionArg[0]);
} else {
  cmd.push(`python${versionArg[1]}`);
}

cmd = cmd.concat(argv);

spawn(cmd.join(' ')).catch(err => {
  if (typeof err.code === 'number') {
    process.exit(err.code);
  } if (typeof err.signal === 'string') {
    console.log(`Process killed by '${err.signal}' signal.`);
    process.exit(1);
  } else {
    console.log(err);
    process.exit(1);
  }
});
