# zaber-python-launcher

[![npm version](https://badge.fury.io/js/%40zaber%2Fpython-launcher.svg)](https://badge.fury.io/js/%40zaber%2Fpython-launcher)

Zaber Python Launcher is a simple utility that executes a particular version of Python on all the platforms (Windows, Linux, Mac OS).

## Install

```
npm install @zaber/python-launcher --save-dev
```

## Usage

The first argument specifies the python version. The rest of the arguments are passed to the python process.

```
npx zaber-python-launcher -3 ...args
npx zaber-python-launcher -2.7 ...args
npx zaber-py -3.5 ...args
```

## Explanation

We sometimes execute Python scripts in our npm scripts.
This becomes difficult when we want to use the specific version of Python on multiple platforms.
For example, on Windows, we need to execute `py -3`, whereas, on Linux, we need `python3`.
This utility calls either `py -X.Y` or `pythonX.Y` depending on the platform along with provided arguments.
